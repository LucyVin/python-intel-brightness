from .brightness import Brightness

def main():
    from sys import argv


    b = Brightness()
    current_pct = (b.brightness/b.brightness_max)*100
    arg = argv[1]

    pct = None
    if arg.lower() == 'up':
        pct = current_pct+5 if current_pct+5 <= b.brightness_max else b.brightness_max

    elif arg.lower() == 'down':
        pct = current_pct-5 if current_pct-5 >= 0 else 0

    elif arg.isdigit():
        if int(arg) > 100 or int(arg) < 0:
            raise BrightnessError('Can not set brightness percentage: {}'.format(arg))

    b.set(pct or int(arg))

if __name__ == '__main__':
    main()
