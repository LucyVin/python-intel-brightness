#! /usr/bin/python


BRIGHTNESS_MAX_FILE = '/sys/class/backlight/intel_backlight/max_brightness'
BRIGHTNESS_FILE = '/sys/class/backlight/intel_backlight/brightness'


class BrightnessError(Exception):
    pass


class Brightness:
    def __init__(self):
        bright_max = open(BRIGHTNESS_MAX_FILE, 'r')
        bright = open(BRIGHTNESS_FILE, 'r')

        self.brightness_max = int(bright_max.read())
        self.brightness = int(bright.read())

        bright_max.close()
        bright.close()

    def set(self, pct):
        pct = int(pct)*.01

        bright_level = int(self.brightness_max*pct)

        bright = open(BRIGHTNESS_FILE, 'w')
        bright.write(str(bright_level))

        self.brightness = bright_level
        return bright_level

