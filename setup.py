from setuptools import setup

setup(
    author='Totooria',
    name='brightness',
    version='0.1',
    packages=['brightness',],
    entry_points={
        'console_scripts':['brightness=brightness:main']
        }
    )
